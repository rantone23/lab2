<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ include file="../jspf/header.jspf" %>
<body class="bg-light text-dark">
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
        <h1 class="display-4 fw-normal">Добро пожаловать</h1>
        <p class="lead fw-normal">Веб сервис созданный для управвления автопарком</p>
    </div>
</div>
</body>
<%@ include file="../jspf/footer.jspf" %>
