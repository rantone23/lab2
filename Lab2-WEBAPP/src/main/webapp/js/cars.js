$(document).ready(function () {
    var exampleModal = document.getElementById('exampleModal')
    exampleModal.addEventListener('show.bs.modal', showCarCreationModal)
});

$(document).ready(function () {
    $("#savingCar").click(function () {
        var car = {
            id: $("#car-id").val(),
            carMark: $("#car-mark").val(),
            carNumber: $("#car-number").val(),
        };
        sendSavingCarRequest(car);
    });
});

function sendSavingCarRequest(car) {
    $.ajax({
        url: "cars",
        method: "POST",
        dataType: "text",
        data: JSON.stringify(car),
        success: function (data) {
            alert("Успешное создание записи");
            window.location.reload();
        },
        error: function (data) {
            console.log(data);
            alert("Произошла ошибка в ходе создания записи");
            window.location.reload();
        }
    })
}

function showCarCreationModal(event) {
    let inputs = $("#exampleModal").find(".modal-body input");
    if (event?.relatedTarget?.id) {
        $("#car-id").val(event.relatedTarget.id);
        $("#car-mark").val($(event.relatedTarget).find(".car-record-mark").text());
        $("#car-number").val($(event.relatedTarget).find(".car-record-number").text());
    } else {
        inputs.each(function () {
            this.value = "";
        });
    }

}

function sendDeletionRequest(carIdentifiers) {
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("DELETE", "cars")
    let request = {
        carIdentifiers: carIdentifiers
    };
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json")
    xmlHttpRequest.addEventListener("load", function (e) {
        if (xmlHttpRequest.status >= 200 && xmlHttpRequest.status < 400) {
            alert("Успешное удаление записей");
            window.location.reload();
        } else {
            alert("Произошла ошибка в ходе удаления записей");
        }

    });
    xmlHttpRequest.send(JSON.stringify(request));
}

function deleteCars() {
    var carIdentifiers = []
    for (const carRecord of document.querySelectorAll(".car-record")) {
        if (carRecord.querySelector("input[type=checkbox]").checked) {
            carIdentifiers.push(carRecord.id);
        }
    }
    sendDeletionRequest(carIdentifiers);
}


